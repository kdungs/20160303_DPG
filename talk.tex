\documentclass[
  aspectratio=1610,
  compress,
  hyperref=pdfusetitle,unicode,
  %professionalfonts
]{beamer}
%\usefonttheme{serif}

\usepackage[italic]{hepnicenames}
%\usepackage{fontspec}
\usepackage{microtype}

\usepackage{polyglossia}
\setmainlanguage{german}
\usepackage{csquotes}

\usepackage{mathtools}
\usepackage[
  math-style=ISO,
  bold-style=ISO,
  partial=upright,
  nabla=upright,
]{unicode-math}
\usepackage{xfrac}

\usepackage{siunitx}
\AtBeginDocument{\sisetup{
  math-rm=\mathup,
  math-micro=µ,
  range-phrase=\text{~--~},
  list-final-separator=\text{~and~},
  list-pair-separator=\text{~and~},
  separate-uncertainty=true
}}

\usepackage{booktabs}
\usepackage{multirow}
\usepackage{colortbl}

\usepackage[font=small]{caption}
\captionsetup[figure]{labelformat=empty}
\captionsetup[subfigure]{labelformat=empty}
\captionsetup[table]{labelformat=empty}

\usepackage{tikz}
% Colors
\definecolor{Gray}{HTML}{444444}
\definecolor{Blue}{HTML}{348ABD}
\definecolor{Purple}{HTML}{7A68A6}
\definecolor{Red}{HTML}{A60628}
\definecolor{Green}{HTML}{467821}
\definecolor{Light red}{HTML}{CF4457}
\definecolor{Teal}{HTML}{188487}
\definecolor{Dark orange}{HTML}{E24A33}
\definecolor{Green TU}{rgb}{0.52 0.72 0.09}
% TikZ flowcharts
\input{flow}  

\usepackage{tcolorbox}

%
% Macros
%
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareRobustCommand{\kw}[1]{\texttt{#1}}
%% Particles and Decays
\DeclareRobustCommand{\bbbar}{\Pbeauty{}\APbeauty}
\DeclareRobustCommand{\Pnonmu}{\text{not}\,\Pmu}
\DeclareRobustCommand{\PKstarzero}{\HepParticle{K}{}{\ast{}0}\xspace}
\DeclareRobustCommand{\Kstmumu}{\HepProcess{\PBzero\rightarrow\PKstarzero\APmuon\Pmuon}}
\DeclareRobustCommand{\JpsiKplus}{\HepProcess{\PBplus\rightarrow\PJpsi\PKplus}}
%% Words
\DeclareRobustCommand{\runone}{Run~I}
\DeclareRobustCommand{\runtwo}{Run~II}
\DeclareRobustCommand{\hlt}{HLT}
\DeclareRobustCommand{\hltone}{\hlt{}1}
\DeclareRobustCommand{\hlttwo}{\hlt{}2}
\DeclareRobustCommand{\velo}{\textsc{Velo}}
%% Units
\DeclareRobustCommand{\gev}{\giga\electronvolt}
\DeclareRobustCommand{\gevc}{\gev\per{}c}
\DeclareRobustCommand{\gevcc}{\gev\per{}c\squared}
\DeclareRobustCommand{\byte}{B}
\DeclareRobustCommand{\teslameter}{\tesla\!\meter}

\titlegraphic{
  \vspace{1cm}
  \includegraphics[height=1cm,keepaspectratio]{logos/cern.pdf}
  \hfill
  \includegraphics[height=1cm,keepaspectratio]{logos/lhcb.pdf}
  \hfill
  \includegraphics[height=1cm,keepaspectratio]{logos/tu.pdf}
  \hfill
  \includegraphics[height=1cm,keepaspectratio]{logos/bmbf.pdf}
}
\title[LHCb µID Optimierung]{Optimierung der softwarebasierten Myonidentifikation am LHCb-Experiment}
\subtitle{\link{https://cds.cern.ch/record/2063310}{CERN-THESIS-2015-181}}
\author[K.~Dungs]{Kevin Dungs}
\institute{CERN \& TU Dortmund}
\date{3. März 2016 \\ DPG Frühjahrstagung 2016 Hamburg}

% --- Beamer options ---
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{bibliography item}[text]
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]
\setbeamercolor{normal text}{fg=Gray}
\setbeamercolor{structure}{fg=Blue}
\setbeamercolor{block title alerted}{fg=Red}
\setbeamercolor{alerted text}{fg=Red}
\setbeamercolor{block title example}{fg=Green}
\setbeamercolor{example text}{fg=Green}
\setbeamertemplate{footline}
{
\leavevmode%
\hbox{%
\begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
\hspace*{2ex}\usebeamerfont{author in head/foot}\insertshortauthor
\end{beamercolorbox}%
\begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
\usebeamerfont{title in head/foot}\insertshorttitle
\end{beamercolorbox}%
\begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
\insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
\end{beamercolorbox}}%
\vskip0pt%
}
% add macro for numbering backup slides
\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}}
}
% ----------------------

\newcommand{\link}[2]{\textcolor{Red}{\footnotesize\href{#1}{#2}}}

\begin{document}
{
  \setbeamertemplate{footline}{}  % get rid of page number on first slide
  \frame{\maketitle}
}

\begin{frame}{Der LHCb Detektor}
  \centering
  \includegraphics[height=0.8\textheight]{figures/lhcbDetector.pdf}
\end{frame}

\begin{frame}{Physik mit Myonen bei LHCb}
  \begin{itemize}
    \item \enquote{Roadmap for selected key measurements of LHCb} (2009) \link{http://arxiv.org/abs/0912.4179}{LHCB-PUB-2009-029} benennt sechs Schlüsselmessungen, drei davon haben Myonen im Endzustand \begin{itemize}
      \item \HepProcess{\PBs\rightarrow\PJpsi\Pphi}
      \item \HepProcess{\PBs\rightarrow\APmuon\Pmuon}
      \item $\Kstmumu$ (z.B. $R_K$)
    \end{itemize}
    \item Diese und viele weitere Analysen protifieren direkt von verbesserter Myonidentifikation
  \end{itemize}
\end{frame}

\begin{frame}{Myonen finden bei LHCb}
  \begin{columns}
    \begin{column}{.5\textwidth}
      Vereinfachtes Flowchart
    \end{column}
    \begin{column}{.5\textwidth}
      \centering
      \includegraphics[keepaspectratio,height=0.8\textheight,width=\textwidth]{figures/muonChambers.pdf}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Effizienzen messen}
\end{frame}

\begin{frame}{Effizienzen in \runone}
\end{frame}

\begin{frame}{Zielsetzung}
\end{frame}

\begin{frame}{Verbesserungen}
\end{frame}

\begin{frame}{Verbesserungen}
\end{frame}

\begin{frame}{Projizierte Effizienzen für \runtwo}
\end{frame}

\begin{frame}{Zusammenfassung}
\end{frame}

\begin{frame}{Randnotiz zu C++}
  \begin{itemize}
    \item C++11/14 ist quasi eine komplett neue Sprache!
    \item Die `STL` ist ein unglaublich mächtiges Werkzeug und Teil von C++!
  \end{itemize}
  \begin{alertblock}{Beispielaufgabe}
    \begin{itemize}
      \item Gegeben eine Methode \kw{hitsAndOccupancies} die für ein gegebenes Suchfenster alle Treffer in den Myonstationen, geordnet nach Station, und die Anzahl an Treffern in jeder Station (\enquote{Occupancy}) extrahiert.
      \item Ein Treffer gilt als gekreuzt wenn er das Predikat \kw{isCrossed} erfuellt.
      \item Berechne alle gekreuzten Treffer und die zugehörigen Occupancies.
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}{Randnotiz zu C++ Teil 2}
  \begin{exampleblock}{Quick and Dirty: 0 Punkte}
    \begin{itemize}
      \item Erweitern von \kw{hitsAndOccupancies} um ein \kw{bool} Flag
      \item Verwende die selbe Funktion zweimal
    \end{itemize}
  \end{exampleblock}

  \begin{itemize}
    \item Extrahiert alle Treffer erneut, was u.U. Decodierung erfordert
    \item Laufzeit ist $\mathcal{O}(n)$ aber nicht besonders Cache-freundlich
    \item Zusätzliche Verzweigung innerhalb einer Schleife
  \end{itemize}
\end{frame}

\begin{frame}{Randnotiz zu C++ Teil 3}
  \begin{exampleblock}{Mit Nachdenken: volle Punktzahl}
    \begin{enumerate}
      \item Filtern der Treffer mit (z.B.) \kw{std::copy\_if(…, isCrossed)} – $\mathcal{O}(n)$
      \item Wiederholte Anwendung von \kw{std::partition\_point} – $\mathcal{O}(\mathrm{log}\,n)$
    \end{enumerate}
    \centering
    \includegraphics[width=0.5\textwidth]{figures/stations.pdf}
  \end{exampleblock}
  \begin{itemize}
    \item Auch $\mathcal{O}(n)$ aber deutlich Cache-freundlicher
    \item Spart erneutes Decodieren!
    \item Lesbarer Code
    \item STL ♥
  \end{itemize}
\end{frame}

% ------
% BACKUP
% ------
\appendix
\backupbegin

{
\setbeamertemplate{footline}{}
\begin{frame}
  \centering
  \vfill
  \Huge{\textsc{\textcolor{Blue}{Backup}}}
  \vfill
\end{frame}
}

\begin{frame}{Erklärung für NShared}
  \centering
  \includegraphics{figures/nShared.pdf}
\end{frame}

\begin{frame}{\runtwo{} Effizienzen}
  \centering
  \includegraphics[width=\textwidth,height=0.9\textheight,keepaspectratio]{figures/effs.pdf}
\end{frame}

\backupend
\end{document}
